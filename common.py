# author: "Jean-Etienne Morlighem <jemorlighem@vegetalsignals.com>"
# date: 2023-01


import statistics


def list_average(l):
    return statistics.mean(l)


def list_median(l):
    return statistics.median(l)


def list_stdev(l):
    return statistics.stdev(l)

